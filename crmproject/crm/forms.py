from django import forms
from crm.models import Contact
from django.contrib.auth.forms import AuthenticationForm,PasswordResetForm,SetPasswordForm

# If you don't do this you cannot use Bootstrap CSS
class LoginForm(AuthenticationForm):
    username = forms.CharField(label="User name", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password'}))


class Password_reset(PasswordResetForm):
    email = forms.EmailField(label="Email Address", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'email'}))
    

class Set_Password(SetPasswordForm):
    password1= forms.CharField(label="Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password1'}))
    password2= forms.CharField(label="Conform Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password2'}))

class ContactForm(forms.ModelForm):
  class Meta:
    model=Contact
    exclude=('created_by','updated_by','shared_to')
      
  
    
