from django.contrib import admin

# Register your models here.
from .models import country
from .models import Address
from .models import Contact
from .models import Communication

admin.site.register(country)
admin.site.register(Address)
admin.site.register(Contact)
admin.site.register(Communication)
