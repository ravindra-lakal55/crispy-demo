from django.apps import AppConfig


class CrmDemoConfig(AppConfig):
    name = 'crm_demo'
