from django.db import models

# Create your models here.



class country(models.Model):
	name = models.CharField(max_length=200)
	abbr= models.CharField(max_length=200)
	default_currency= models.CharField(max_length=200)
	created_by= models.CharField(max_length=200)
	updated_by= models.CharField(max_length=200)


class Address(models.Model):
	addressline1 = models.CharField(max_length=200)
	addressline2= models.CharField(max_length=200)
	city= models.CharField(max_length=200)
	country_id= models.CharField(max_length=200)
	city= models.CharField(max_length=200)
	postal_code= models.CharField(max_length=200)
	created_by= models.CharField(max_length=200)
	updated_by= models.CharField(max_length=200)

class Contact(models.Model):
	Salutation = models.CharField(max_length=200)
	FirstName= models.CharField(max_length=200)
	LastName= models.CharField(max_length=200)
	Title= models.CharField(max_length=200)
	Gender= models.CharField(max_length=200)
	Age= models.CharField(max_length=200)
	DateOfBirth= models.CharField(max_length=200)
	MaritalStatus= models.CharField(max_length=200)
	Communication_id= models.CharField(max_length=200)
	created_by= models.CharField(max_length=200)
	updated_by= models.CharField(max_length=200)
	shared_to= models.CharField(max_length=200)

class Communication(models.Model):
	email_primary = models.CharField(max_length=200)
	email_others= models.CharField(max_length=200)
	city= models.CharField(max_length=200)
	work_tel= models.CharField(max_length=200)
	home_tel= models.CharField(max_length=200)
	fax= models.CharField(max_length=200)
	mobile= models.CharField(max_length=200)
	facebook= models.CharField(max_length=200)
	google_plus= models.CharField(max_length=200)
	facebook= models.CharField(max_length=200)
	linkedin= models.CharField(max_length=200)
	website= models.CharField(max_length=200)
	instagram= models.CharField(max_length=200)
	pintrest= models.CharField(max_length=200)
	created_by= models.CharField(max_length=200)
	updated_by= models.CharField(max_length=200)



